from selenium.webdriver import Firefox
# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.firefox import options
from selenium.webdriver.firefox.options import Options
# from selenium.webdriver.common.keys import Keys
# import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementClickInterceptedException, NoSuchElementException, TimeoutException
import requests

option = Options()
option.headless = True
driver = Firefox(options=option)

class esewa:

    def __init__(self):
        driver.get('https://esewa.com.np/')

        WebDriverWait(driver,10).until(EC.presence_of_all_elements_located((By.NAME,'username')))

        WebDriverWait(driver,10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR,'.es-bank-partner-section')))
        user = driver.find_element_by_name('username')
        password = driver.find_element_by_name('password')
        user.send_keys('')#username
        password.send_keys('')#password

        WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR,'button.btn:nth-child(1)')))
        login = driver.find_element_by_css_selector('button.btn:nth-child(1)')
        login.click()


        all_cookies = driver.get_cookies()
        WebDriverWait(driver,10).until(EC.element_to_be_clickable((By.CSS_SELECTOR,"div.col-lg-6:nth-child(3)")))
        driver.quit()
        cookies = ""
        xsrf_token = ""
        for i in range(len(all_cookies)):
            cookies += "{0}={1};".format(all_cookies[i]['name'],all_cookies[i]['value'])
            if all_cookies[i]['name'] ==  'XSRF-TOKEN':
                xsrf_token = all_cookies[i]['value']
        self.headers = {"Cookie":cookies,"X-XSRF-TOKEN-ES":xsrf_token,"User-Agent":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0"}
       



    def verify(self,bank_code,account_number,account_holder_name):
            payload = {"bank_code":bank_code,"account_number":account_number,"account_holder_name":account_holder_name}
            r = requests.post(url="https://esewa.com.np/api/web/auth/bank/withdraw/validate",headers=self.headers,json=payload,timeout=50)
            return r.json()




verification = esewa()
# print(verification.verify("GLBBNPKA","C807010002532","Gaurav Giri"))
