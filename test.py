#ThreadLess

from esewa.esewa import verification
import requests
import time

banklist=[{'id': 1, 'bank_code': 'ADBLNPKA'}, {'id': 2, 'bank_code': 'BOKLNPKA'}, {'id': 3, 'bank_code': 'BFCLNPKA'}, {'id': 4, 'bank_code': 'CCBNNPKA'}, {'id': 5, 'bank_code': 'CTZNNPKA'}, {'id': 6, 'bank_code': 'CIVLNPKA'}, {'id': 7, 'bank_code': 'EVBLNPKA'}, {'id': 8, 'bank_code': 'GRDBLNPKA'}, {'id': 9, 'bank_code': 'GLBBNPKA'}, {'id': 10, 'bank_code': 'GRENNPKA'}, {'id': 11, 'bank_code': 'KMFNPKA'}, {'id': 12, 'bank_code': 'ICFCNPKA'}, {'id': 13, 'bank_code': 'JBBLNPKA'}, {'id': 14, 'bank_code': 'KSKFNPKA'}, {'id': 15, 'bank_code': 'KRDBLNPKA'}, {'id': 16, 'bank_code': 'KMBLNPKA'}, {'id': 17, 'bank_code': 'LXBLNPKA'}, {'id': 18, 'bank_code': 'HNBNPKA'}, {'id': 19, 'bank_code': 'MBLNNPKA'}, {'id': 20, 'bank_code': 'MBBLNPKA'}, {'id': 21, 'bank_code': 'MFILNPKA'}, {'id': 22, 'bank_code': 'MBNLNPKA'}, {'id': 23, 'bank_code': 'MNBBLNPKA'}, {'id': 24, 'bank_code': 'NARBNPKA'}, {'id': 25, 'bank_code': 'NPBBNPKA'}, {'id': 26, 'bank_code': 'NEBLNPKA'}, {'id': 27, 'bank_code': 'NBOCNPKA'}, {'id': 28, 'bank_code': 'NIBLNPKA'}, {'id': 29, 'bank_code': 'NSBINPKA'}, {'id': 30, 'bank_code': 'BOALNPKA'}, {'id': 31, 'bank_code': 'NMBBNPKA'}, {'id': 32, 'bank_code': 'PFLNPKA'}, {'id': 33, 'bank_code': 'KISTNPKA'}, {'id': 34, 'bank_code': 'PCBLNPKA'}, {'id': 35, 'bank_code': 'PFLTDNPKA'}, {'id': 36, 'bank_code': 'RBBANPKA'}, {'id': 37, 'bank_code': 'RFLNPKA'}, {'id': 38, 'bank_code': 'SNMANPKA'}, {'id': 39, 'bank_code': 'SKDBLNPKA'}, {'id': 40, 'bank_code': 'SGBBNPKA'}, {'id': 41, 'bank_code': 'SRDBNPKA'}, {'id': 42, 'bank_code': 'SINDNPKA'}, {'id': 43, 'bank_code': 'SFLNPKA'}, {'id': 44, 'bank_code': 'SRBLNPKA'}, {'id': 45, 'bank_code': 'TBBNPKA'}, {'id': 46, 'bank_code': 'UFLNPKA'}]

userlist = requests.get('http://localhost/api/userbanklist').json()


data = []

def get_data():
    for user in userlist:
        try:
            idie = user['id']
            name = user['name']
            account_number = user['account_number']
            bank_code = banklist[user['bank_id']-1]['bank_code']
            verification_status = verification.verify(bank_code,account_number,name)
            # print(verification_status)
            payload = {
                'id': idie,
                'status': verification_status['code']
            }
            data.append(payload)
            if payload['status'] == 0:
                print( 'id:{0} | status:verified'.format(payload['id']))
            else:
                print('id:{0} | status:not verified'.format(payload['id']))

        except:
            print("Bad Internet")
                
            
    datas = {"data":data}
    r = requests.post('http://localhost/api/verify',json=datas)
        

if __name__ == '__main__':
    get_data()